import numpy as np
import pydicom
from pydicom.dataset import Dataset, FileDataset


class Dicom:
    @staticmethod
    def write(filename, image, PatientName, PatientID, ContentDate, ContentTime, PatientComments):
        file_meta = Dataset()
        file_meta.MediaStorageSOPClassUID = 'Secondary Capture Image Storage'
        file_meta.MediaStorageSOPInstanceUID = '1.3.6.1.4.1.9590.100.1.1.111165684411017669021768385720736873780'
        file_meta.ImplementationClassUID = '1.3.6.1.4.1.9590.100.1.0.100.4.0'

        ds = FileDataset(filename, {},
                         file_meta=file_meta, preamble=b"\0" * 128)

        ds.is_little_endian = True
        ds.is_implicit_VR = True

        ds.SOPClassUID = '1.2.840.10008.5.1.4.1.1.12.1'

        ds.PatientName = PatientName
        ds.PatientID = PatientID
        ds.PatientComments = PatientComments

        ds.ContentDate = ContentDate
        ds.ContentTime = ContentTime

        ds.SamplesPerPixel = 1
        ds.PhotometricInterpretation = "MONOCHROME2"
        ds.PixelRepresentation = 0
        ds.HighBit = 7
        ds.BitsStored = 8
        ds.BitsAllocated = 8
        ds.Columns = image.shape[0]
        ds.Rows = image.shape[1]
        if image.dtype != np.uint8:
            image = image.astype(np.uint8)
        ds.PixelData = image.tobytes()
        ds.file_meta.TransferSyntaxUID = pydicom.uid.ImplicitVRLittleEndian
        ds.save_as(filename)
        return image

    def read(filename):
        ds = pydicom.dcmread(filename)
        # pixels = list(ds.PixelData)
        # pixelsIterator = 0
        # shape = (ds.Columns, ds.Rows)
        # print(shape, len(pixels))
        # result_img = np.zeros(shape).astype(np.uint16)
        #
        # for i in range(ds.Columns):
        #     for j in range(ds.Rows):
        #         result_img[j][i] += pixels[pixelsIterator]
        #         pixelsIterator += 2

        return ds
